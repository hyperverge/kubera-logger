const request = require('request');

const defaultIdExtractor = req => `${req.headers.appid}-${req.headers.appkey}`;

const defaultShouldCheckRateLimit = (appId) => {
  if (appId) return true; // for eslint
  return true; // if shouldCheckRateLimit is not passed we need to perform rate limit check always
};

const rateLimiter = (
  product = null,
  idFunc = defaultIdExtractor,
  shouldCheckRateLimit = defaultShouldCheckRateLimit,
) => (req, res, next) => {
  const appId = idFunc(req);
  if (shouldCheckRateLimit(appId)) {
    request.post({
      url: 'https://kbr-ind0.api.hyperverge.co/api/v1/rates',
      timeout: 1000,
      body: {
        id: appId,
        product,
        path: req.path,
      },
      headers: {
        'X-Request-Id': req.id,
      },
      json: true,
    }, (error, response) => {
      if (error) return next();

      if (response.statusCode === 429) {
        return res.status(429).json({
          statusCode: 429,
          status: 'failure',
          message: 'Requests rate limit exceeded',
        });
      }
      return next();
    });
  }
  return next();
};

module.exports = rateLimiter;
