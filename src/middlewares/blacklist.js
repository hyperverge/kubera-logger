const deepmerge = require('deepmerge');

const addBlacklist = (blacklist = {}) => (req, res, next) => {
  req._blacklist = deepmerge(req._blacklist || {}, blacklist);
  res._blacklist = deepmerge(res._blacklist || {}, blacklist);
  next();
};

module.exports = { addBlacklist };
