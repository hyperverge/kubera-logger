const logRequest = (req, res, next) => {
  req._kuberaWhitelist = true;
  next();
};
module.exports = logRequest;
