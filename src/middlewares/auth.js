const request = require('request');
const jwt = require('jsonwebtoken');
const md5 = require('md5');
const fs = require('fs');
const crypto = require('crypto');

const cachedCredentials = {};
const authUrl = 'https://auth.hyperverge.co/credentials/authenticate';

/**
 * Method to initialize auth middleware
 * @param publicCertPath: absolute path to pubic hashing key for verifying JWT token
 * @param logger: kubera-logger instance
 * @param errStatusCode: Status code to send in case Auth fails
 * @param validity: Validity of a credential(milliseconds)
 * in cache from the time of insertion in cache
 * @returns {Function}
 */
module.exports = (
  publicCertPath, logger, errStatusCode = 401, validity = 15 * 60 * 1000,
) => {
  if (!publicCertPath) {
    throw new Error('No Public Certificate File Passed');
  }
  if (!logger) {
    throw new Error('No Logger Passed');
  }
  const publicCert = fs.readFileSync(publicCertPath);

  const authAPI = (appId, appKey, authToken, cb) => {
    let headers = {};
    let jsonBody = {};
    if (appId && appKey) {
      jsonBody = {
        appId,
        appKey,
      };
    }
    if (authToken) {
      headers = {
        Authorization: authToken,
      };
    }
    request({
      method: 'POST',
      url: authUrl,
      headers,
      json: jsonBody,
      timeout: 2000,
    }, (err, response, body) => {
      if (err) {
        logger.log(logger.LOG_LEVELS.ERROR, `Error while calling authentication api ${JSON.stringify(err)}. Not blocking the request`);
        return cb(null);
      }
      if (response.statusCode === 200) {
        return cb(null, body.result);
      }
      if (response.statusCode !== 401) {
        logger.log(logger.LOG_LEVELS.ERROR, `Non 401 status code ${response.statusCode} from Auth server. Hence not blocking the request`);
        return cb(null);
      }
      if (body.error === 'Token Expired') {
        logger.log(logger.LOG_LEVELS.ERROR, 'Authentication failed as token has expired');
        return cb({ err: 'TOKEN_EXPIRED', message: 'Token Expired' });
      }
      logger.log(logger.LOG_LEVELS.ERROR, `Authentication failed. Response Status code: ${response.statusCode} and Response body: ${JSON.stringify(body)}`);
      return cb({ err: 'UNAUTHORIZED', message: 'Missing/Invalid credentials' });
    });
  };

  return (req, res, next) => {
    let appId = req.headers.appid || req.body.appId || null;
    const appKey = req.headers.appkey || req.body.appKey || null;
    const authToken = req.headers.authorization || null;
    if ((!appId || !appKey) && !authToken) {
      logger.log(logger.LOG_LEVELS.WARN, 'Missing appId and appKey or authorization header');
      return next({ status: errStatusCode, message: 'Missing/Invalid credentials' });
    }
    if (authToken) {
      let decoded = null;
      try {
        decoded = jwt.verify(authToken.substring(7), publicCert, { json: true });
      } catch (err) {
        if (err.name === 'TokenExpiredError') {
          logger.log(logger.LOG_LEVELS.ERROR, 'JWT Token Expired');
          return next({ status: errStatusCode, message: 'Token Expired' });
        }
        logger.log(logger.LOG_LEVELS.ERROR, `Error while verifying JWT token. Error: ${err}`);
        return next({ status: errStatusCode, message: 'Invalid token' });
      }
      ({ appId } = decoded);
      req.headers.appid = appId;
    }
    if (!cachedCredentials[appId] || cachedCredentials[appId].expiry <= new Date().getTime()) {
      // Do Auth and add to cache
      return authAPI(appId, appKey, authToken, (err, result) => {
        if (!err) {
          const appKeyDigest = result ? result.appKeyDigest : crypto.createHash('md5').update(appKey).digest('hex');
          req.headers.appkeydigest = appKeyDigest;
          cachedCredentials[appId] = {
            appKeyDigest,
            expiry: new Date().getTime() + validity,
          };
          delete req.headers.appkey;
          return next();
        }
        return next({ status: errStatusCode, message: err.message });
      });
    }
    if (authToken || cachedCredentials[appId].appKeyDigest === md5(appKey)) {
      req.headers.appkeydigest = cachedCredentials[appId].appKeyDigest;
      delete req.headers.appkey;
      return next();
    }
    return next({ status: errStatusCode, message: 'Missing/Invalid credentials' });
  };
};
