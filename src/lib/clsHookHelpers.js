const { createNamespace } = require('cls-hooked');

const writer = createNamespace('request-id-session');

const getRequestId = () => writer.get('requestId');

const clsHookSessionMiddleware = (req, res, next) => {
  writer.bindEmitter(req);
  writer.bindEmitter(res);
  writer.run(() => {
    writer.set('requestId', req.id);
    next();
  });
};

const overWriteRequestId = (requestId) => {
  writer.set('requestId', requestId);
};

module.exports = {
  getRequestId,
  clsHookSessionMiddleware,
  overWriteRequestId,
};
