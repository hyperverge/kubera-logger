const uuid = require('uuid');

const generateId = base => `${Math.floor(new Date())}-${base}`;

module.exports = (options) => {
  options = options || {};
  options.uuidVersion = options.uuidVersion || 'v4';
  options.setHeader = options.setHeader === undefined || !!options.setHeader;
  options.headerName = options.headerName || 'X-Request-Id';
  options.attributeName = options.attributeName || 'id';

  return (req, res, next) => {
    req[options.attributeName] = req.header(options.headerName)
      || generateId(uuid[options.uuidVersion](options, options.buffer, options.offset));
    if (options.setHeader) {
      res.setHeader(options.headerName, req[options.attributeName]);
    }
    next();
  };
};
