/* eslint no-caller:off */
/* eslint no-restricted-properties:off */
Object.defineProperty(global, '__stack', {
  get() {
    const orig = Error.prepareStackTrace;
    Error.prepareStackTrace = (_, stack) => stack;
    const err = new Error();
    Error.captureStackTrace(err, arguments.callee);
    const { stack } = err.stack;
    Error.prepareStackTrace = orig;
    return stack;
  },
});
