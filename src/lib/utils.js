const jp = require('jsonpath');

const maskedBody = (body, blacklists, mask) => {
  if (body instanceof Object) {
    [].concat(blacklists).forEach((rule) => {
      jp.apply({ ...body }, rule, (val) => {
        if (val || val === false) return mask;
        return val;
      });
    });
  }
  return body;
};

const getIp = req => req.headers['x-forwarded-for'] || req.connection.remoteAddress;

module.exports = {
  maskedBody,
  getIp,
};
