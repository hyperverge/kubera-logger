const winston = require('winston');
// require('../lib/stacktrace');// adds stack trace to __stack variable
const clsHookHelper = require('../lib/clsHookHelpers');

const MESSAGE = Symbol.for('message');

const LOG_LEVEL_SEVERITY = {
  fatal: 1,
  error: 2,
  warn: 3,
  trace: 4,
  info: 5,
  debug: 6,
};

const LOG_LEVELS = {
  FATAL: 'fatal',
  ERROR: 'error',
  WARN: 'warn',
  TRACE: 'trace',
  INFO: 'info',
  DEBUG: 'debug',
};

const hvLogFormat = winston.format((info) => {
  const infoCopy = Object.assign({}, info);
  infoCopy[MESSAGE] = `${new Date().toISOString()} ${info.level.toUpperCase()} [${info.message.instanceId}] [${info.message.product}] [${info.message.processName}] [${info.message.fileName}] [${info.message.methodName}] [${info.message.requestId}] ${info.message.message}`;
  return infoCopy;
})();

const getDefaultLogFilePath = (processName = 'node') => `/var/log/HV/node/${processName}.log`;

const appLogger = function logger(instanceId, product, processName, app,
  shouldUseClsForTracing = true, logFile) {
  // ensuring minimum supported version to be node 10
  if (Number(process.version.split('.')[0].substring(1)) < 10) {
    return null;
  }

  this.LOG_LEVELS = LOG_LEVELS;

  // TODO figure out when to disable async_hook usng asycnHooks.disable()
  if (app && shouldUseClsForTracing) {
    app.use(clsHookHelper.clsHookSessionMiddleware);
  }

  let instance = this;
  if (appLogger.prototype._singletonInstance) {
    instance = appLogger.prototype._singletonInstance;
  } else {
    instance.logger = winston.createLogger({
      format: hvLogFormat,
      levels: LOG_LEVEL_SEVERITY,
    });
  }
  appLogger.prototype._singletonInstance = instance;

  instance.instanceId = instanceId || instance.instanceId;
  instance.product = product || instance.product;
  instance.processName = processName || instance.processName;
  instance.logFile = logFile || instance.logFile || getDefaultLogFilePath(instance.processName);
  const fileTransport = new winston.transports.File({
    filename: instance.logFile,
    level: LOG_LEVELS.DEBUG,
  });
  instance.logger
    .clear()
    .add(fileTransport);

  instance.log = (loglevel, message, requestId) => {
    const logMsg = {
      instanceId: instance.instanceId,
      product: instance.product,
      processName: instance.processName,
      requestId: requestId || clsHookHelper.getRequestId(),
      message,
      level: loglevel,
      fileName: 'unknown',
      methodName: 'unknown',
    };
    instance.logger.log({
      level: loglevel,
      message: logMsg,
    });
  };

  if (shouldUseClsForTracing) {
    instance.getRequestId = clsHookHelper.getRequestId;
    instance.overWriteRequestId = clsHookHelper.overWriteRequestId;
  }
  return instance;
};

module.exports = appLogger;
