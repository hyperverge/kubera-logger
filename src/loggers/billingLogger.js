const winston = require('winston');
const expressWinston = require('express-winston');

const billingLog = (req, res) => ({
  requestId: req.id || '-',
  statusCode: res.statusCode,
  originalUrl: req.originalUrl || '-',
  event_timestamp: new Date().toISOString(),
  event_module: req.headers.module,
  reference_id: req.headers.referenceid || '-',
  transaction_id: req.headers.transactionid || '-',
  appid: req.headers.appid || '-',
});

const billingFormatter = winston.format((info, opts) => {
  const { addDefaults, removeDefaults } = opts;
  Object.keys(addDefaults).forEach((k) => { info[k] = addDefaults[k]; });
  removeDefaults.forEach((k) => {
    if (info[k]) {
      delete info[k];
    }
  });
  if (info.message) {
    Object.keys(info.message).forEach((key) => {
      info[key] = info.message[key];
    });
    delete info.message;
  }
  return info;
});

const billingLogger = (app, product, opts = {}) => {
  const filename = opts.filename || `/var/log/HV/billing/${product}.log`;
  const { format } = winston;
  const wLogger = winston.createLogger({
    transports: [
      new winston.transports.File({
        filename,
      }),
    ],
    format: format.combine(
      billingFormatter({
        addDefaults: { product },
        removeDefaults: ['level', 'meta'],
      }),
      format.json(),
    ),
  });

  const logger = expressWinston.logger({
    winstonInstance: wLogger,
    colorize: false,
    meta: false,
    msg: billingLog,
    skip(req) {
      return !req.headers.module;
    },
  });

  app.use(logger);
};

module.exports = billingLogger;
