const winston = require('winston');
const expressWinston = require('express-winston');
const addRequestId = require('../lib/requestId.js')();
const utils = require('../lib/utils');
const pjson = require('../../package.json');
const { addBlacklist } = require('../middlewares/blacklist.js');

const isBlacklisted = (blacklistPathsRegex, url) => {
  const marchUrl = blacklistPath => blacklistPath.test(url);
  return blacklistPathsRegex.some(marchUrl);
};

const requestLogger = (app, product, opts = {}) => {
  const ipFunction = opts.getIp || utils.getIp;
  const filename = opts.filename || `/var/log/HV/requests/${product}.log`;
  const mask = opts.mask || '--masked--';
  const extraProperties = opts.extraProperties || {};
  const blacklistPaths = opts.blacklistPaths || [];
  const blacklistPathsRegex = blacklistPaths.map(path => new RegExp(path));

  const whitelistPaths = opts.whitelistPaths || [];
  const whitelistPathsRegex = whitelistPaths.map(path => new RegExp(path));

  expressWinston.requestWhitelist.push('body', 'id', ...(extraProperties.request || []));
  expressWinston.responseWhitelist.push('body', ...(extraProperties.response || []));

  const { format } = winston;

  const wLogger = winston.createLogger({
    transports: [
      new winston.transports.File({
        json: true,
        filename,
      }),
    ],
    format: format.combine(format.timestamp(), format.json()),
  });

  const logger = expressWinston.logger({
    winstonInstance: wLogger,
    meta: true,
    expressFormat: true,
    colorize: false,
    requestFilter(req, propName) {
      const blacklist = req._blacklist;
      if (propName === 'body' && ((blacklist || {}).request || {}).body) {
        return utils.maskedBody(req[propName], blacklist.request.body, mask);
      }
      if (propName === 'headers' && ((blacklist || {}).request || {}).headers) {
        return utils.maskedBody(req[propName], blacklist.request.headers, mask);
      }
      return req[propName];
    },
    responseFilter(res, propName) {
      const blacklist = res._blacklist;
      if (propName === 'body' && ((blacklist || {}).response || {}).body) {
        return utils.maskedBody(res[propName], blacklist.response.body, mask);
      }
      return res[propName];
    },
    dynamicMeta(req) {
      return {
        product,
        ip: ipFunction(req),
        libVersion: pjson.version,
        route: (req.route || {}).path,
      };
    },
    ignoreRoute(req) {
      return isBlacklisted(blacklistPathsRegex, req.originalUrl);
    },
    skip(req) {
      return !req._kuberaWhitelist && !isBlacklisted(whitelistPathsRegex, req.originalUrl);
    },
  });

  app.use(addRequestId);
  app.use(addBlacklist(opts.blacklist || {}));
  app.use(logger);
};

module.exports = { requestLogger, isBlacklisted };
