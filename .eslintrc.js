module.exports = {
    "extends": "airbnb-base",
    "rules": {
      "no-underscore-dangle": "off",
      "no-param-reassign": "off",
    },
    "overrides": [
      {
          "files": ["test/*.js"],
          "rules": {
              "no-unused-expressions": "off"
          }
      }
  ]
};
