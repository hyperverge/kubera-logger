# NodeJS Express module for Kubera integration

- Generates request-response logs and stores them in a file.
- Adds X-Request-Id in case it doesn't exist
- Adds a middleware to handle rate-limiting

## Installation
`npm install --save https://gitlab.com/hyperverge/kubera-logger`

*** IMPORTANT UPGRADE NOTE: If you are updating from 1.5.x or prior. The requests are not logged
by default and extra configuration needs to be made to explicitly log it ***

## RequestLogger
- Generates request-response logs and stores them in a file.
- Adds X-Request-Id in case it doesn't exist

### Usage
To make use of request logger, you need to add the following to your application

```js
//ES6
const { requestLogger } = require('kubera-logger');
```

```js
//ES5
var requestLogger = require('kubera-logger').requestLogger;
```

**Before** the routes are added to the express app
```js
requestLogger(app, product, opts);
```
Where:
- app: Express app instance
- product: Name of the product (eg 'india-kyc')
- opts: additional options (see below)

### Options
```js
opts = {
  whitelistPaths: [ '/path/to/log' ],
  blacklist: {
    response: {
      body: ['list of jsonpaths to', 'keys to be masked', '$.result[*].details.aadhaar'],
    },
    request: {
      body: ['list of jsonpaths'],
      headers: ['list of jsonpaths'],
    },
  },
  extraProperties: {
    request: ['extra', 'properties', 'to log in', 'request'],
    response: ['extra', 'properties', 'to log in', 'response'],
  },
  filename: '/path/to/log/files', // default is `/var/log/HV/requests/<product>.log`
  getIp: function(req) { return String; }, // Custom function to get ip from request object if required
  mask: 'string to replace sensitive keys', // default is --masked--
}
```
See [jsonpath](https://github.com/dchester/jsonpath#readme) for syntax

### Log Request

There are two ways to mark routes to be logged.

1. Using a middleware
2. Using globally defined route whitelist

#### Middleware

Import the middleware `logRequest`

```js
const { logRequest } = require('kubera-logger');
```

Add it as a middleware to whatever routes you want to get logged. Please don't add it to '/' as
that defeats the purpose. You can use the second way to whitelist routes if things are getting complicated.

#### Whitelist Options

Use the `whitelistPaths: []` in the initializing config of `requestLogger`

eg

```js
{
  whitelistPaths: [ '/v1/readKYC', /v2.0/ ],
}
```

Again, be as specific as you can so as to avoid random requests like `/v2.0/admin.php` to be logged.


## Application Logger
Application logger should be used as the single point from where app logs should be written by node applications. The logs written by this logger follow standard format across HyperVerge and hence is by default supported by Kubera ELK setup

### Pre-requisite
* Node 10 or higher

### Usage

#### Initialization
```js
//ES6
const { appLogger } = require('kubera-logger');
const logger = new appLogger(instanceId, productId, processName, app, shouldUseClsForTracing, logFile)
```

```js
//ES5
var logger = require('kubera-logger').appLogger(instanceId, productId, processName, logFilePath);
```
where:
* **instanceId** is the unique identifier of the machine/instance.
* **productId** is the identifier of the product for which logs are being logged
* **processName** is the name of the process in the product mentioned above. For eg: nodeapp
* **app** is express app instance and is optional
* **shouldUseClsForTracing** expects a boolean value and determines if CLS should be used to add requestId to each log.
This again is optional and if not set atleast once during any instance of logger initialization would lead to logs not having requestId.
Default value: `true`
* **logFilePath** is the path to the log file. Default value: `/var/log/HV/node/{processName}.log`

#### Logging
In order to write logs in the standard format supported by Kubera ELK setup, call `log` function on `logger` object
```js
logger.log(logger.LOG_LEVELS.DEBUG, message, requestId);
```
where:
* first parameter specifies the log level. Valid values are:
	* DEBUG
	* INFO
	* TRACE
	* WARN
	* ERROR
	* FATAL
* message means the log message that needs to be logged,
* requestId is an optional parameter and if passed will add the requestId to the log. This will override the requestId 
that is saved in CLS session(if enabled during initialization in last step)

#### Get Request Id for current Async context
```js
logger.getRequestId();
```

#### Log Format
This logger prints log in following format:
```
2018-09-07T14:32:48.411IST INFO [hostname] [product] [process] [file:line] [calling function] [request id] Log Message
```

#### Other Details
* This logger follows Singleton Design Pattern. Hence only one instance will be shared across all requires/imports.
Hence once initialized, it need not be initialized.
Just a require `require('kubera-logger').appLogger()` is enough to re-use it post initialization.
* The parameters used for intialization of appLogger can be changed anytime by reinitializing with new parameters
* Understanding **requestId logging for tracing**:
    * To enable tracing using requestId, during intialization of appLogger, it should be ensured that
    express app instance is passed and `shouldUseClsForTracing` is true.
    * Before initialization, requestLogger should be initialized or a unique value should be set to `id` key in `req` object.
    If not done, appLogger won't be able to save the requestId to the session.
    * App Logger uses [cls-hooked](https://www.npmjs.com/package/cls-hooked) node module for maintaining requestId in session.
    This module uses `Async Hook` which is an experimental feature available from Node 8. A lot of external dependencies
    (mongoose, bluebird etc) break the session which leads to persisted session getting purged and hence tracing doesn't work.
    Workarounds for some of the common dependencies can be found below:
        * **Mongoose**: [Plugin](https://github.com/Automattic/mongoose/issues/5929) to make CLS work.
        [Fork of mongoose](https://github.com/Automattic/mongoose/issues/2987) supporting CLS.

    * ##### Workaround for CLS Hook getting messed up because of libraries not supporting Async Hooks:
        *  Manually add the requestId into the session:
            ```js
            logger.overWriteRequestId(requestId);
            ```
        *  Emit all before and after around asynchronous functions([reference](https://github.com/nodejs/help/issues/1021#issuecomment-355241045)):

            This will ensure that context is maintained around async calls for libraries breaking the context. 
            ```js
            function doSomething(callback) {
                const resource = new AsyncResource('rethinkdb-query');
                asyncFunction((asyncResult) => {
                    resource.emitBefore();
                    callback
                    resource.emitAfter();
                })
            };
            ```
            Note: In node 10, emitBefore and emitAfter have been deprecated.

        There might me other ways to handle CLS mess up. Another possible way can be found here [here](https://github.com/nodejs/help/issues/1021#issuecomment-355248190)

## Rate Limiting

### Installation and Usage
Rate limiting is part of the library module as an express middleware that has to be injected for the relevant routes
```js
const { rateLimiter } = require('kubera-logger');
```

It is a function that returns a middleware that can be then injected in the relevant controllers. (I know I'm repeating this a lot, but it's important)

```js
// For example
app.use(rateLimiter('product-name'), idGenerator);
```

Where:
- product-name is the name of the product
- idGenerator is a function that accepts the request object and returns the unique identifier for the client/user

An example of idGenerator is:
```js
const idGenerator = (req) => {
   return req.headers.appid + '-' + req.headers.appkey
}
```
Which is also the default way of generating id in case no function is provided

It is difficult to provide a universal step-by-step process of integrating this middleware but here is a case that'll work for most.

1. Make a common middleware file where the rate-limiter functions can be defined `eg: app/routes/commonMiddlewares.js`
2. Implement and export the rateLimiter (be sure to provide the product name as that is the major reason this process has so many steps)
3. Import and add the exported rate limiter in the relevant routes after auth and before the controller function
```js
// For example

// --- app/routes/commondMiddlerwares.js ---
const { rateLimiter } = require('kubera-logger');

exports.myRateLimiter = rateLimiter('myProductName', (req) => Algo.amaze(req));


// --- app/routes/v9001/index.js ---

const { myRateLimiter } = require('@routes/commonMiddlewares');
router.get('/', myRateLimiter, (req, res) => {
  return res.json({escape: 'the queue'});
});
```

## Auth: Caching & Local Token Verification

### Installation and Usage
Auth is part of the library module as an express middleware that has to be injected for the relevant routes
```js
const { auth } = require('kubera-logger');
```

It is a function that returns a middleware that can be then injected in the relevant controllers.

```js
// Usage in app.js
app.use(auth(publicKeyPath, appLogger, errStatusCode, validity));

// Usage with a specific route
app.post('/xyx', auth(publicKeyPath, appLogger, errStatusCode, validity));
```

Where:
- publicKeyPath is the local absolute path of Public Key which will be used for token validation
- appLogger is an instance of [appLogger](#application-logger)
- errStatusCode is the value of property `status` in error passed to `next` in case of Auth Failure. Default value for this is 401
- validity is the expiry time in milliseconds from the time of caching a credential with default being 15 minutes

### Auth Failure
Auth can fail for any of following reasons:
- Token has expired
- Credentials are wrong
- Credentials from which token has been created is deleted

In case of failure, Auth middleware will pass the error to next function. The error object passed will have status and message as fields.
Status will be same as passed `errStatusCode` and message will have relevant message that can be passed back to clients via response.

Sample error handling code for auth and other internal error should look like:
```js
const out = require('@lib/apiout');
app.use(function(err, req, res, next) {
  if (err.message) {
    // log error
  }
  return out.error(res, err.status || 500, err.message || 'Internal Server Error');
});
```
