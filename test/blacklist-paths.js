
const chai = require('chai');

const { isBlacklisted } = require('../src/loggers/requestLogger');

const blacklistPaths = [
  '^/v(1|1.1)/hello$',
];
const blacklistPathsRegex = blacklistPaths.map(path => new RegExp(path));

// eslint-disable-next-line no-undef
describe('blacklist paths test', () => {
  // eslint-disable-next-line no-undef
  it('check if paths get blacklisted', () => {
    chai.expect(isBlacklisted(blacklistPathsRegex, '/v1/hello')).to.be.true;
    chai.expect(isBlacklisted(blacklistPathsRegex, '/v1.1/hello')).to.be.true;
  });

  // eslint-disable-next-line no-undef
  it('check if paths do not get blacklisted', () => {
    chai.expect(isBlacklisted(blacklistPathsRegex, 'yolo/v1/hello/yolo')).to.be.false;
    chai.expect(isBlacklisted(blacklistPathsRegex, 'yolo/v1/hello')).to.be.false;
    chai.expect(isBlacklisted(blacklistPathsRegex, 'v1/hello/yolo')).to.be.false;
  });
});
